import textwrap
import dataset

db = dataset.connect("postgresql://localhost:5432/language_tagging_db", reflect_views=True)
table = db["valid_sample_set_large"]


for record in table.all():
    if len(record['snippet']) < 600:
        continue

    new_text = textwrap.wrap(record['snippet'], 300)

    record['snippet'] = new_text.pop(0)

    for snippet in new_text:
        table.insert({ 'snippet': snippet, 'language': record['language'] })

    table.update(record, ['id'])
