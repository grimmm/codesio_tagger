import tensorflow as tf
from tensorflow import keras as k
from tensorflow.keras import backend as K
import numpy as np
import pandas as pd
import re
import faker
import datetime
import os.path
from IPython import embed
from gensim.models import Word2Vec
LANGUAGES = ["bash", "c", "c#", "c++", "java", "javascript", "matlab", "objective-c", "perl", "php", "python", "r", "ruby", "scala"]

def prepare_samples_for_use(df, name):
    if os.path.isfile("./prepared_%s_x_set.df" % name):
        df = pd.read_pickle("./prepared_%s_x_set.df" % name)
    else:
        df = df.apply(lambda x: remove_unwanted_characters(x))
        df = translate_text_to_indices(df)
        df.to_pickle("./prepared_%s_x_set.df" % name)

    return np.array(df.tolist())

def prepare_labels_for_use(df, name):
    if os.path.isfile("./prepared_%s_y_set.df" % name):
        df = pd.read_pickle("./prepared_%s_y_set.df" % name)
    else:
        df = encode_labels_for_classification(df)
        df.to_pickle("./prepared_%s_y_set.df" % name)

    df_values = df.values

    return k.utils.to_categorical(df_values, len(LANGUAGES))


x_test = prepare_samples_for_use(None, "test")
y_test = prepare_labels_for_use(None, "test")

model = k.models.load_model("./models/Lime_2018-10-29_02:42:55.168531)

performance_metrics = model.evaluate(x_test, y_test, batch_size=256)

print(performance_metrics)
