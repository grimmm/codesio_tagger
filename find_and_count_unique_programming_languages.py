import csv
import hashlib
import json
import matplotlib.pyplot as plt
from collections import Counter
import numpy as np
from pprint import pprint

languages = set()
languages_only = set()
def format_tag(tag):
    return tag.lstrip('{').rstrip('}').lower().strip()

with open("./languages.csv", "r") as f:
    csv_lines = csv.reader(f)
    for line in csv_lines:
        lang = line[0].lstrip().rstrip().lower()
        languages.add(lang)
        languages_only.add(lang)

languages = Counter(languages)

with open("./tag_sets.csv", "r") as f:
    reader = csv.reader(f)

    for line in reader:
        elements = format_tag(line[0]).split(",")

        for ele in elements:
            if ele in languages_only:
                languages[ele] += 1
most_common = languages.most_common(60)
labels, values = zip(*most_common)
indexes = np.arange(len(labels))
width = 1

plt.figure(figsize=(20,20))
ax = plt.gca()
plt.bar(indexes, values, width)
plt.xticks(indexes, labels, rotation=30)
for i,v in enumerate(values):
    ax.text(v+3, i + 0.25, str(v), color="black", fontweight="bold")
plt.show()

pprint(most_common)
